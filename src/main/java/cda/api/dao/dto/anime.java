package cda.api.dao.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.io.Serializable;

@Entity
public class anime {
    @Id
    private int ID;
    private String titre;
    private boolean nature;
    private String genre;
    private String studio_animation;
    private String date_sortie;
    private String synopsis;
    private String lien_image;
    private String lien_video;

    /* GETTER / SETTER */

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean isNature() {
        return nature;
    }

    public void setNature(boolean nature) {
        this.nature = nature;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStudio_animation() {
        return studio_animation;
    }

    public void setStudio_animation(String studio_animation) {
        this.studio_animation = studio_animation;
    }

    public String getDate_sortie() {
        return date_sortie;
    }

    public void setDate_sortie(String date_sortie) {
        this.date_sortie = date_sortie;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getLien_image() {
        return lien_image;
    }

    public void setLien_image(String lien_image) {
        this.lien_image = lien_image;
    }

    public String getLien_video() {
        return lien_video;
    }

    public void setLien_video(String lien_video) {
        this.lien_video = lien_video;
    }
}
