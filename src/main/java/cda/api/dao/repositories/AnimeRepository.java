package cda.api.dao.repositories;

import cda.api.dao.dto.anime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimeRepository extends JpaRepository<anime, Integer> {
    @Query(value = "SELECT * FROM anime WHERE genre = :genre AND id != :ID LIMIT 3", nativeQuery = true)
    List<anime> findThreeByGenre(@Param("genre") String genre, @Param("ID") Integer id);
}
